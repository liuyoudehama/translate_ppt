# Slide 19

人工智能与数据计算平台

Artificial Intelligence and Data Computing Platform

（1）支持国产异构计算资源海光、昇腾、寒武纪等

(1) Supports domestic heterogeneous computing resources such as Haiguang, Ascend, Cambricon, etc.

（2）容器化全流程支撑服务

(2) Fully supports containerized end-to-end support services

（3）支持联邦学习等技术

(3) Supports technologies like federated learning

（4）全链路生态一站式研发能力在线AI软件开发、调试和计算学科领域的模型、数据和应用

(4) Full-stack ecosystem for online AI software development, debugging, and computing in model, data, and application fields

（5）平台技术解决方案，应用到院内外多家单位

(5) Platform technology solutions applied to multiple units both inside and outside the institution

统一接入高效管理

Unified access and efficient management