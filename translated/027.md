## Slide 27

### 案例：材料设计与发现
**Case Study: Materials Design and Discovery**

### 材料数据隐私计算
*Material Data Privacy Computing*

### 材料结构预测软件
*Material Structure Prediction Software*

### 材料数据库存储规范和APIVASP软件的内容自动提取支持20个“材料基因”专项材料数据汇交任务
*Material database storage specifications and automatic content extraction of APIVASP software supporting 20 "material gene" special material data submission tasks*

### 材料数据汇交系统
*Material Data Submission System*

### Scientific Data, 9(2022):787 
*Scientific Data, 9(2022):787*

### 材料特征工程算法
*Material Feature Engineering Algorithm*

### 基于神经网络与先验知识混合的材料属性特征筛选发展高精度物理解析特征计算方法支持超导、量子材料筛选
*Development of high-precision physical analytical feature calculation methods for material property feature selection based on neural networks and mixed prior knowledge supporting superconductors, quantum material selection*

### Science China Materials, 2022专利：一种基于数据驱动的材料性质预测方法及系统(CN113505527B)
*Science China Materials, 2022 patent: A data-driven method and system for predicting material properties (CN113505527B)*

### 晶体结构预测软件群体智能优化算法电池材料预测
*Crystal structure prediction software, collective intelligence optimization algorithm, battery material prediction*

### Computational Materials Science, 214(2022):111699
*Computational Materials Science, 214(2022):111699*

### Energy Storage Materials, 54(2023):403-409
*Energy Storage Materials, 54(2023):403-409*

### 提出材料科学的隐私计算解决方案实现材料数据的安全共享与高效利用
*Proposing privacy computing solutions for materials science to achieve secure sharing and efficient utilization of material data*

### Scientific Reports, 12(2022):15326
*Scientific Reports, 12(2022):15326*

### 模型训练
*Model Training*

### 数据应用
*Data Application*

### 模型应用
*Model Application*

### 数据入库
*Data Storage*

### 数据应用
*Data Application*

### 模型训练
*Model Training*

### 开展人工智能与材料学、晶体结构预测
*Conducting artificial intelligence in materials science, crystal structure prediction*

### AI
*AI*