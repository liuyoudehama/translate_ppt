# Slide 12

支撑世界科技前沿

Supporting the forefront of global science and technology

支持天文、流体力学、理论物理等领域多项前沿科学计算任务

Supporting multiple cutting-edge scientific computing tasks in astronomy, fluid mechanics, theoretical physics, and other fields

PHoToNS支撑宇宙大尺度结构模拟应用

PHoToNS supports cosmological large-scale structure simulation applications

国台大规模天文N体模拟软件PHoToNS完成3万亿暗物质粒子模拟当前领域规模最大、精度最高的黎明时期暗物质演化过程的模拟

The large-scale astronomical N-body simulation software PHoToNS at National Taiwan University has completed a simulation of 30 trillion dark matter particles, the largest and most accurate simulation of the dawn era dark matter evolution in the current field.

超大规模可压缩湍流直接数值模拟

Ultra-large-scale compressible turbulent direct numerical simulation

力学所首次实现对升力体外形的DNS计算，对超声速和高超声速的压缩折角的超百亿网格的直接数值模拟，计算能力提升一个量级，计算周期由数月缩短到几天之内揭示了飞行器湍流、转捩、分离及激波干扰的新现象及新机理，获得中国空气动力学会科学技术一等奖。

For the first time, the Institute of Mechanics achieved DNS calculations of lifting body shapes, conducting direct numerical simulations on ultra-billion-grid compressible angled shocks at hypersonic and high hypersonic speeds. This advancement elevated computing capabilities by an order of magnitude, reducing calculation times from months to a few days, uncovering new phenomena and mechanisms in aircraft turbulence, transition, separation, and shock interference, earning the Chinese Aerodynamics Research Society's First Prize in Science and Technology.

上亿自由度量子场论系统的高精度经典模拟

High-precision classical simulations of quantum field theory systems with tens of millions of degrees of freedom

理论物理所自主实现了基于杨-米尔斯规范场的强相互作用完整数据集，初步摆脱了对国外数据集的依赖基于最大50亿自由度的大规模并行计算，精确预言了高能对撞机实验所需的强子内部结构信息，国际上第一次通过数值模拟验证了核子质量主要来自于强相互作用量子反常。

Theoretical Physics Institute autonomously achieved a complete dataset on strong interaction based on Yang-Mills gauge field, preliminarily breaking dependence on foreign datasets. Using massive-scale parallel computing based on up to 5 billion degrees of freedom, it accurately predicted internal structure information of hadrons required for high-energy collider experiments. Internationally, it's the first time nuclear mass has been verified through numerical simulation to mainly originate from quantum anomalies in strong interactions.
