# Slide 7

**发展超算自主核心技术，建设国产超算应用生态**  
Develop Independent Core Technologies for Supercomputing, Build Domestic Supercomputing Application Ecosystem

**高性能计算技术与应用**  
High-Performance Computing Technology and Applications

**人工智能技术与应用**  
Artificial Intelligence Technology and Applications

**可视化技术与应用**  
Visualization Technology and Applications

**超算中心运行与应用服务**  
Supercomputing Center Operations and Application Services

**新一代国产异构超级计算机（东方、元、人工智能平台）**  
New Generation of Domestic Heterogeneous Supercomputers ("ORISE", "Yuan", AI Platform)

**后处理**  
Post-processing

**框架软件**  
Framework Software

**共性技术研究**  
Common Technology Research

**应用软件研制**  
Application Software Development

**资源生态环境**  
Resource Ecology and Environment

**天文**  
Astronomy

**业务单元**  
Business Units

**研发核心技术和代表性科学计算应用软件，支撑国家科技创新发展**  
Develop Core Technologies and Representative Scientific Computing Application Software to Support National Scientific and Technological Innovation Development

**框架软件的完善和应用推广**  
Engineering Excellence of Framework Software and Promotion of Applications

**算法库研发与应用**  
Algorithm Library Development and Application

**并行优化技术研究与应用**  
Parallel Optimization Technology Research and Application

**大气、海洋、天文、电磁、有限元仿真**  
Simulation of Atmosphere, Ocean, Astronomy, Electromagnetic and Finite Element

**超算互联网平台软件**  
Supercomputing Platform as a service