from pptx import Presentation
import markdownify as md

def extract_text_from_shape(shape):
    """Recursively extract text from a shape and its children."""
    text = ""
    if hasattr(shape, "text_frame") and shape.text_frame is not None:
        for paragraph in shape.text_frame.paragraphs:
            for run in paragraph.runs:
                text += run.text
    return text

def pptx_to_markdown(pptx_path, markdown_path):
    # Load the presentation
    prs = Presentation(pptx_path)
    
    # Initialize a string to hold the markdown content
    markdown_content = ""
    
    # Iterate through slides and extract text
    for slide_number, slide in enumerate(prs.slides):
        markdown_content += f"# Slide {slide_number + 1}\n\n"
        for shape in slide.shapes:
            text = extract_text_from_shape(shape)
            if text:
                markdown_content += md.markdownify(text) + "\n\n"
            # Handle grouped shapes
            if shape.shape_type == 6:  # 6 corresponds to a group shape
                for sub_shape in shape.shapes:
                    text = extract_text_from_shape(sub_shape)
                    if text:
                        markdown_content += md.markdownify(text) + "\n\n"
            # Handle tables
            if shape.shape_type == 19:  # 19 corresponds to a table
                table = shape.table
                for row in table.rows:
                    for cell in row.cells:
                        text = extract_text_from_shape(cell)
                        if text:
                            markdown_content += md.markdownify(text) + "\n\n"
    
    # Write the markdown content to a file
    with open(markdown_path, 'w', encoding='utf-8') as f:
        f.write(markdown_content)

# Example usage
pptx_path = "raw.pptx"
markdown_path = "example2.md"
pptx_to_markdown(pptx_path, markdown_path)
