# Slide 18

高性能数值算法库软件SC\_HCAL

（1）集成了多类型的共性数值并行算法库以及求解器，能适应于多种异构计算机体系结构特征值问题预条件和稀疏线性方程组快速算法（快速多极子、稀疏矩阵近似逆等）（2）中科院超级计算环境、无锡超算、中国科技大学、武汉大学、中山大学以及美国劳伦斯伯克利国家实验室

序号

算法/软件包名称（英文名)

主要功能简述

1

PSEPS(Parallel Symmetric Eigenvalue Problem Software)

用于并行求解稠密矩阵特征值问题及SVD问题

2

PLOBPCG(Parallel Local Optimal BLOCK Preconditioned Conjugate Gradient )

基于并行局部最优块预条件共轭梯度方法LOBPCG的算法库求解器，用于并行求解稀疏对称矩阵特征值问题及相关问题

3

PFEAST(Parallel FEAST)

基于FEAST方法的算法库求解器，用于并行求解稀疏矩阵特征值问题及相关问题

4

PJD（Parallel Jacobi and Davidson） 

基于Jacobi-Davidson的算法库求解器，用于并行求解稀疏矩阵特征值问题及相关问题

5

HSLES(Parallel System of Linear Equations )

可用于稀疏线性代数方程组并行求解，提供了可扩展Krylov子空间迭代法法、BiCGSTab迭代方法和CG迭代法及其相应求解器器

6

ScaPre（Scalable Preconditioned Library）

可扩展预条件并行算法库，提供了包括代数多重网格AMG、近似逆SSOR-AI和SPAI-AINV等多个预条件方法实现，主要用于线性方程组和特征值问题求解
