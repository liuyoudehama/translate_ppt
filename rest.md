# Slide 27

案例：材料设计与发现

材料数据隐私计算

材料结构预测软件

材料数据库存储规范和APIVASP软件的内容自动提取支持20个“材料基因”专项材料数据汇交任务

材料数据汇交系统

Scientific Data, 9(2022):787 

材料特征工程算法

基于神经网络与先验知识混合的材料属性特征筛选发展高精度物理解析特征计算方法支持超导、量子材料筛选

Science China Materials, 2022专利：一种基于数据驱动的材料性质预测方法及系统(CN113505527B)

晶体结构预测软件群体智能优化算法电池材料预测

Computational Materials Science, 214(2022):111699Energy Storage Materials, 54(2023):403-409

提出材料科学的隐私计算解决方案实现材料数据的安全共享与高效利用

Scientific Reports, 12(2022):15326

模型训练

数据应用

模型应用

数据入库

数据应用

模型训练

开展人工智能与材料学、晶体结构预测

AI

# Slide 28

Computer Network Information Center, Chinese Academy of Sciences

中国科学院计算机网络信息中心

衷心感谢！

