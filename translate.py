from pptx import Presentation
import markdownify as md

def pptx_to_markdown(pptx_path, markdown_path):
    # Load the presentation
    prs = Presentation(pptx_path)
    
    # Initialize a string to hold the markdown content
    markdown_content = ""
    
    # Iterate through slides and extract text
    for slide_number, slide in enumerate(prs.slides):
        markdown_content += f"# Slide {slide_number + 1}\n\n"
        for shape in slide.shapes:
            if hasattr(shape, "text"):
                markdown_content += md.markdownify(shape.text) + "\n\n"
    
    # Write the markdown content to a file
    with open(markdown_path, 'w', encoding='utf-8') as f:
        f.write(markdown_content)

# Example usage
pptx_path = "raw.pptx"
markdown_path = "example.md"
pptx_to_markdown(pptx_path, markdown_path)
