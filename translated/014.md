Here's the translation of Slide 14 while preserving the original Chinese:

---

# Slide 14

技术支持新药研制、农作物等多项生命科学领域计算任务

Technical support for drug development, agriculture, and other computational tasks in the life sciences field

高性能大尺度小麦基因组重构

High-performance large-scale wheat genome reconstruction

快速产生高质量普通小麦基因组组装及注释结果LarGo软件对比业内商业软件和开源软件具有更好的性能、精确性和结果突破了科农 9204 小麦没有完整基因注释的局限性

Rapid generation of high-quality common wheat genome assembly and annotation results. LarGo software compared with industry commercial and open-source software demonstrates better performance, accuracy, and breakthrough results, overcoming limitations in the incomplete gene annotation of Kome 9204 wheat.

支撑人民生命健康

Supporting public health

超大规模虚拟筛选加速抗新冠病毒药物攻关

Large-scale virtual screening accelerates research on anti-COVID-19 drug candidates

虚拟筛选时间缩短50%以上超快筛选近20亿小分子 （已报道速度最快、规模最大）融合若干药物小分子数据库和自有库交互可视动力学模拟、在线结果分析入选 ‘十三五’科技创新成就展”、选科技部重点研发计划“高性能计算”专项优秀成果（10/39）

Virtual screening time reduced by over 50%, rapidly screening nearly 2 billion small molecules (reported as the fastest and largest scale). Integration with several drug small molecule databases and proprietary libraries for interactive visual dynamics simulation and online result analysis selected for the "13th Five-Year Plan" Science and Technology Innovation Achievement Exhibition, and the Ministry of Science and Technology's Key R&D Program "High-Performance Computing" special excellent results (10/39).

超级计算助力帕金森病药物靶点的结构研究

Supercomputing assists in structural studies of drug targets for Parkinson's disease

通过动力学计算对Nurr1-RXRα-DNA复合物模型分析，获得动态结构信息为以Nurr1以及Nurr1-RXRα为靶点的帕金森药物的研究提供理论基础。

Dynamic structural information obtained through kinetic calculations of the Nurr1-RXRα-DNA complex model provides a theoretical basis for the study of Parkinson's drugs targeting Nurr1 and Nurr1-RXRα.

---