# Slide 11

“东方”超算系统运行情况

Operation Status of "ORISE" Supercomputing System

≥4097 CPU规模作业占用29.8%计算机时

≥4097 CPU-scale jobs occupy 29.8% of computer time

≥ 1025DCU规模作业占用42.9%计算机时

≥ 1025 DCU-scale jobs occupy 42.9% of computer time

2023年7月至2024年5月，539个用户提交并完成约259.4万个作业，使用4.3亿CPU小时、2698.3万DCU小时；系统平均利用率69%，最高利用率81.2%；涉及生物信息学、计算宇宙学、基因组学、地球系统科学等计算。

From July 2023 to May 2024, 539 users submitted and completed approximately 2.594 million jobs, using 430 million CPU hours and 26.983 million DCU hours; system average utilization rate 69%, peak utilization rate 81.2%; involving bioinformatics, computational cosmology, genomics, Earth system science, and other computations.

31.4% growth Jul 2023 -> May 2024

