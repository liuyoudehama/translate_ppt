# Slide 21

重点应用领域大规模应用软件

Key Application Areas of Large-Scale Application Software

地球系统模式CAS-ESM

Earth System Model CAS-ESM

材料微组织演化大规模模拟软件ScETD-PF

Large-scale simulation software for material microstructure evolution ScETD-PF

面向工程应用的大规模流场模拟软件CCFD

Large-scale flow field simulation software for engineering applications CCFD

多组学分析算法与软件

Multi-omics analysis algorithms and software

大规模计算宇宙学N体模拟软件PhotoNS

Large-scale computational cosmology N-body simulation software PhotoNS

分子动力学模拟应用软件eMD

Molecular dynamics simulation application software eMD