发展超算自主核心技术，建设国产超算应用生态
Develop independent supercomputing core technologies and build a domestic supercomputing application ecosystem

高性能计算技术与应用
High-performance computing technology and applications

人工智能技术与应用
Artificial intelligence technology and applications

可视化技术与应用
Visualization technology and applications

超算中心运行与应用服务
Supercomputing center operations and application services

新一代国产异构超级计算机（东方、元、人工智能平台）
Next-generation domestic heterogeneous supercomputers (Dongfang, Yuan, AI platforms)

后处理
Post-processing

框架软件
Framework software

共性技术研究
General technology research

应用软件研制
Application software development

资源生态环境
Resource ecosystem environment

天文
Astronomy

业务单元
Business unit

研发核心技术和代表性科学计算应用软件，支撑国家科技创新发展
Develop core technologies and representative scientific computing application software to support national scientific and technological innovation and development

框架软件的完善和应用推广
Framework software improvement and application promotion

算法库研发与应用
Algorithm library development and application

并行优化技术研究与应用
Parallel optimization technology research and application

大气、海洋、天文、电磁、有限元仿真
Atmospheric, ocean, astronomy, electromagnetic, and finite element simulation

超算互联网平台软件
Supercomputing internet platform software

Slide 8
汇报提纲
Report Outline

二. 超算环境
2. Supercomputing Environment

一. 中心简介

Center Introduction
技术研发
Technical Research and Development

Slide 9
排名
Rank

机构
Institution

国家
Country

系统
System

处理器核/个
Processor Cores

浮点速度PFlop/s
Floating Point Speed PFlop/s

架构
Architecture

1
橡树岭国家实验室
Oak Ridge National Laboratory

美国
USA

Frontier

8,699,904

1194.00

X86 CPU+GPU

2
阿贡国家实验室
Argonne National Laboratory

美国
USA

Aurora

4,742,808

585.34

X86 CPU+GPU

3
微软Azure云
Microsoft Azure Cloud

美国
USA

Eagle

1,123,200

561.20

X86 CPU+GPU

4
理研计算科学中心
Riken Center for Computational Science

日本
Japan

Fugaku

7,630,848

442.01

ARM CPU

5
芬兰国家超级计算中心
CSC – IT Center for Science

芬兰
Finland

LUMI

2,752,704

379.70

X86 CPU+GPU

6
瑞士国家超算中心
Swiss National Supercomputing Centre

瑞士
Switzerland

Alps

1,305,600

270.00

X86 CPU+GPU

7
意大利CINECA超算中心
CINECA Supercomputing Center

意大利
Italy

Leonardo

1,824,768

238.70

X86 CPU+GPU

8
橡树岭国家实验室
Oak Ridge National Laboratory

美国
USA

Summit

2,414,592

148.60

Power9+GPU

9
巴塞罗那超级计算中心
Barcelona Supercomputing Center

西班牙
Spain

MareNostrum 5 ACC

680,960

138.2

X86 CPU+GPU

10
英伟达公司
NVIDIA Corporation

美国
USA

Eos NVIDIA DGX SuperPOD

485,888

121.4

X86 CPU+GPU

11
劳伦斯利弗莫尔国家实验室
Lawrence Livermore National Laboratory

美国
USA

Sierra

1,572,480

94.64

Power9+GPU

东方”超算系统——计算性能国际领先
Dongfang Supercomputing System - Leading International Computational Performance

“东方”超级计算系统（ 200P，北京）
Dongfang Supercomputing System (200P, Beijing)

我院首个C类先导专项攻坚芯片“卡脖子” 技术研制成果总投资超10亿元
Our institute's first Class C pioneering project to tackle the "chokepoint" technology development of chips with a total investment of over 1 billion yuan

中国科学院超级计算中心第七代系统
Seventh-generation system of the Chinese Academy of Sciences Supercomputing Center

国家超算（中国科学院）中心业务主机
National Supercomputing (Chinese Academy of Sciences) Center Business Host

发挥我院建制化优势，集应用领域、数学和计算机科学多学科交叉团队
Leverage our institute's structured advantages, integrating multidisciplinary teams in application fields, mathematics, and computer science

东方（ORISE）
Dongfang (ORISE)

2020年3月，经中国高性能计算机性能TOP100排行榜专家组鉴定，HPL实测性能119.6PF，HPCG实测性能2.89PF，超过当时第二的美国Sierra系统（2024年6月排第11）
In March 2020, certified by the expert panel of the China High-Performance Computer Performance TOP100 list, the HPL measured performance was 119.6 PF, and the HPCG measured performance was 2.89 PF, surpassing the then-second American Sierra system (ranked 11th in June 2024)

系统性能国际领先
System performance is internationally leading

Slide 10
“东方”超算系统——软件生态建设
Dongfang Supercomputing System - Software Ecosystem Construction

院先导B专项：适配国产异构计算系统的先进计算方法与软件
Institute's Pioneering B Project: Advanced Computing Methods and Software for Domestic Heterogeneous Computing Systems

依托单位：中国科学院计算机网络信息中心
Host Unit: Computer Network Information Center, Chinese Academy of Sciences

院先导C专项：国产安全可控先进计算系统研制
Institute's Pioneering C Project: Development of Domestic Secure and Controllable Advanced Computing Systems

应用
Applications

应用效果
Application Effects

对标国际
Benchmarking International

国际先进水平
International Advanced Level

钛合金
Titanium Alloy

持续性能77.5P，42.35%峰值，较入选GB Finalist提升52%。入选GB奖水平（CCF）
Sustained performance 77.5P, 42.35% peak, a 52% improvement over the GB Finalist entry. Reached GB Award level (CCF)

领跑
Leading

51PF

纳米器件
Nano Devices

硅纳微结构与器件的计算模拟，6500节点。入选GB奖水平（CCF）
Simulation of silicon nanostructures and devices with 6500 nodes. Reached GB Award level (CCF)

并跑
Parallel

85.45PF(双精度)90.89PF(混合精度)
85.45PF (double precision) 90.89PF (mixed precision)

格点QCD
Lattice QCD

用5184节点，强扩展性最大做到963×192的格子，国际最大规模的格点量子色动力学模拟，有助微观粒子物理量的更精准计算
Using 5184 nodes, strong scalability achieving a 963×192 lattice, the largest scale lattice quantum chromodynamics simulation in the world, aiding in more precise calculations of microscopic particle physics quantities

领跑
Leading

963×192的格子
963×192 lattice

药物筛选
Drug Screening

6000节点，6000CPU+24000DCU，50%效率，最强筛选能力16-18亿分子/天。科技部“全国科技系统抗击新冠肺炎疫情先进集体和先进个人”
6000 nodes, 6000 CPUs + 24000 DCUs, 50% efficiency, the strongest screening capability of 1.6-1.8 billion molecules/day. Recognized by the Ministry of Science and Technology as "National Advanced Collective and Individual in the Fight Against COVID-19"

领跑
Leading

160000 CPU，10亿分子/天
160000 CPUs, 1 billion molecules/day

海洋模式
Ocean Model

用6500个节点，最大规模LICOM模式计算，全球平均分辨率3km，10模式年
Using 6500 nodes, the largest scale LICOM model computation, global average resolution of 3km, 10 model years

并跑
Parallel

全球平均分辨率2km，几模式天
Global average resolution of 2km, several model days

天文
Astronomy

已知最大粒子规模1.48万亿（114003），用6000个节点
The largest known particle scale of 1.48 trillion (114003), using 6000 nodes

并跑
Parallel

10PF

CFD
Computational Fluid Dynamics (CFD)

5.39千亿网格规模，24576块DCU卡，模拟时间步长比传统显/隐格式提升10000/100倍，模拟规模比国外团队提升20倍（2016）和120倍（2019）
5.39 trillion grid scale, 24576 DCU cards, simulation time step increased by 10000/100 times compared to traditional explicit/implicit formats, simulation scale increased by 20 times (2016) and 120 times (2019) compared to foreign teams

并跑
Parallel

P数未知
Unknown P number