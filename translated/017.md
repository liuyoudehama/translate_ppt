# Slide 17

高性能跨平台可移植并行框架软件

Design and implement a unified programming interface and memory management scheme targeting domestic heterogeneous hardware. Initial development completion supports SC\_Tangram V2.0 to enable code portability and performance across platforms including X86 CPUs, ARM CPUs, GPUs, DCUs, and the Sunway many-core architecture. Applications extend to oceanography and cryptography fields.

框架软件设计架构

Framework software design architecture

密码学算法在不同平台上性能测试

Performance testing of cryptographic algorithms on different platforms

密码学算法

Cryptographic algorithms

全球（1/20°）的LICOM3模拟逐日海表涡动动能（EKE）

Daily surface eddy kinetic energy (EKE) simulation of global (1/20°) LICOM3

海洋模式应用示范

Demonstration of ocean model applications